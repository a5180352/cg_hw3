#include "triangle.hpp"

bool triangle::hit(const ray &_ray, float t_min, float t_max, hit_record &rec) const {
    // write something here ...
    // source : https://www.scratchapixel.com/lessons/3d-basic-rendering/ray-tracing-rendering-a-triangle/moller-trumbore-ray-triangle-intersection
    vec3 orig = _ray.origin();
    vec3 dir = _ray.direction();
    static const float kEpsilon = std::numeric_limits<float>::epsilon();

    vec3 v0v1 = v1 - v0;
    vec3 v0v2 = v2 - v0;
    vec3 pvec = cross(dir, v0v2);
    float det = dot(v0v1, pvec);

    // if the determinant is negative the triangle is backfacing
    // if the determinant is close to 0, the ray misses the triangle
    if (det < kEpsilon) {
        return false;
    }
    // ray and triangle are parallel if det is close to 0
    // if (fabsf(det) < kEpsilon) {
    //     return false;
    // }

    float invDet = 1 / det;

    vec3 tvec = orig - v0;
    rec.u = dot(tvec, pvec) * invDet;
    if (rec.u < 0.0f || rec.u > 1.0f) {
        return false;
    }

    vec3 qvec = cross(tvec, v0v1);
    rec.v = dot(dir, qvec) * invDet;
    if (rec.v < 0.0f || rec.u + rec.v > 1.0f) {
        return false;
    }

    rec.t = dot(v0v2, qvec) * invDet;
    if (rec.t < t_min || rec.t > t_max) {
        return false;
    }

    rec.mat_ptr = triangle_mat;
    rec.normal = normal;
    rec.p = _ray.point_at_parameter(rec.t);

    return true;
}