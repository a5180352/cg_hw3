#include "transform.hpp"

vec3 Translate(vec3 target, vec3 displacement) {
    // write something here ...
    return target + displacement;
}

vec3 Rotate(vec3 target, vec3 axis, float angle) {
    // write something here ...
    float angle_rad = angle * pi / 180.0f;
    float cos8 = cosf(angle_rad), sin8 = sinf(angle_rad); // 8 means theta
    vec3 unit_axis = unit_vector(axis);
    // source : https://en.wikipedia.org/wiki/Rotation_matrix#Rotation_matrix_from_axis_and_angle
    return target * cos8 + cross(unit_axis, target) * sin8 + dot(unit_axis, target) * unit_axis * (1 - cos8);
}

bool transform::hit(const ray &r, float t_min, float t_max, hit_record &rec) const {
    // write something here ...
    vec3 new_origin = Rotate(Translate(r.origin(), -displacement), rotateAxis, -rotateAngle);
    vec3 new_direct = Rotate(r.direction(), rotateAxis, -rotateAngle);
    bool is_hit = target->hit(ray(new_origin, new_direct), t_min, t_max, rec);
    if (is_hit) {
        rec.p = Translate(Rotate(rec.p, rotateAxis, rotateAngle), displacement);
        rec.normal = Rotate(rec.normal, rotateAxis, rotateAngle);
    }
    return is_hit;
}