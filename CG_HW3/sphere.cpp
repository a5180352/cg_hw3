#include "sphere.hpp"

bool sphere::hit(const ray &r, float t_min, float t_max, hit_record &rec) const {
    // write something here ...
    // ax^2 + bx + c = 0 : dot(B, B)*t^2 + 2*dot(B, A-C)*t + dot(A-C, A-C) - R^2 = 0
    float a = dot(r.direction(), r.direction());
    float b = 2 * dot(r.direction(), r.origin() - center);
    float c = dot(r.origin() - center, r.origin() - center) - radius * radius;
    // delta = b^2 - 4ac
    float delta = b * b - 4 * a * c;
    if (delta > 0.0f) {
        // root 1 : closer one
        float t = (-b - sqrtf(delta)) / (2 * a);
        if (t > t_min && t < t_max) {
            rec.t = t;
            rec.p = r.point_at_parameter(t);
            rec.normal = (rec.p - center) / radius;
            rec.mat_ptr = mat_ptr;
            // texture coordinate on sphere (uv mapping) : https://en.wikipedia.org/wiki/UV_mapping
            vec3 p_to_center = center - rec.p;
            rec.u = 0.5 + (atan2f(p_to_center.z(), p_to_center.x()) / (2 * pi));
            rec.v = 0.5 - (asinf(p_to_center.y()) / pi);
            return true;
        }
        // root 2
        t = (-b + sqrtf(delta)) / (2 * a);
        if (t > t_min && t < t_max) {
            rec.t = t;
            rec.p = r.point_at_parameter(t);
            rec.normal = (rec.p - center) / radius;
            rec.mat_ptr = mat_ptr;
            // texture coordinate on sphere (uv mapping) : https://en.wikipedia.org/wiki/UV_mapping
            vec3 p_to_center = center - rec.p;
            rec.u = 0.5 + (atan2f(p_to_center.z(), p_to_center.x()) / (2 * pi));
            rec.v = 0.5 - (asinf(p_to_center.y()) / pi);
            return true;
        }
    }
    return false;
}