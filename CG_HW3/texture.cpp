#include "texture.hpp"

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

image_texture::image_texture(const char *filepath) {
    // write something here ...
    // Hint: load image to variable "data"
    data = stbi_load(filepath, &width, &height, &channel, 0);
    // int x, y, n;
    // unsigned char *data = stbi_load(filename, &x, &y, &n, 0);
    // ... process data if not NULL ...
    // ... x = width, y = height, n = # 8-bit components per pixel ...
    // ... replace '0' with '1'..'4' to force that many components per pixel
    // ... but 'n' will always be the number that it would have been if you said 0
}

vec3 image_texture::value_at_uv(float _u, float _v, const vec3 &_hit_point) const {
    // write something here ...
    int x = int(width  * _u * 2) % width;
    int y = int(height * _v * 2) % height;
    int index = (y * width + x) * channel;
    return vec3(data[index + 0] / 255.0, data[index + 1] / 255.0, data[index + 2] / 255.0);
}

vec3 constant_texture::value_at_uv(float _u, float _v, const vec3 &_hit_point) const {
    return color;
}