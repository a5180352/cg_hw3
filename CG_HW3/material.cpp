#include "material.hpp"
#include "random.hpp"

// reflect function
vec3 reflect(const vec3 &d, const vec3 &n) {
    return d - 2 * dot(d, n) * n;
}

static inline bool refract(const vec3 &v, const vec3 &n, float ni_over_nt, vec3 &refracted) {
    vec3 uv = unit_vector(v);
    float dt = dot(uv, n);
    float discriminant = 1.0 - ni_over_nt * ni_over_nt * (1 - dt * dt);
    if (discriminant > 0) {
        refracted = ni_over_nt * (uv - n * dt) - n * sqrt(discriminant);
        return true;
    } else {
        return false;
    }
}

static inline float schlick(float cosine, float ref_idx) {
    float r0 = (1 - ref_idx) / (1 + ref_idx);
    r0 = r0 * r0;
    return r0 + (1 - r0) * pow((1 - cosine), 5);
}

vec3 material::emitted() const {
    // material that is not light don't returns energy
    return vec3(0, 0, 0);
}

bool lambertian::scatter(const ray &r_in, hit_record &rec, vec3 &attenuation, ray &scattered) const {
    // write something here ...
    // S = (P + N) + r : a random target that points outward the surface
    vec3 S = rec.p + rec.normal + unit_vector(vec3(drand48() - 0.5f, drand48() - 0.5f, drand48() - 0.5f));
    scattered = ray(rec.p, S - rec.p);
    attenuation = tex_ptr->value_at_uv(rec.u, rec.v, rec.p);
    return true;
}

bool metal::scatter(const ray &r_in, hit_record &rec, vec3 &attenuation, ray &scattered) const {
    // write something here ...
    vec3 r = reflect(unit_vector(r_in.direction()), rec.normal);
    // rand_vec() return a random vector with length less than 1 (set as 0.8 here)
    scattered = ray(rec.p, r + fuzz * unit_vector(vec3(drand48() - 0.5f, drand48() - 0.5f, drand48() - 0.5f)) * 0.8);
    attenuation = albedo;
    return true;
}

// bool dielectric::scatter(const ray &r_in, hit_record &rec, vec3 &attenuation, ray &scattered) const {
//     // write something here ...
//     attenuation = vec3(1, 1, 1);
//     // source : https://www.scratchapixel.com/lessons/3d-basic-rendering/introduction-to-shading/reflection-refraction-fresnel
//     vec3 I = unit_vector(r_in.direction()), N = rec.normal;
//     float cos1 = -dot(N, I);
//     float n1 = 1.0, n2 = ref_idx;
//     if (cos1 < 0) { // I is inside
//         cos1 = -cos1;
//     } else {
//         std::swap(n1, n2);
//         N = -N;
//     }
//     float n = n1 / n2;
//     float cos2_square = 1 - n * n * (1 - cos1 * cos1);
//     if (cos2_square > 0) { // refraction
//         vec3 T = n * I + (n * cos1 - sqrtf(cos2_square)) * N;
//         scattered = ray(rec.p, T);
//     } else { // total reflection
//         vec3 R = reflect(I, N);
//         scattered = ray(rec.p, R);
//     }
//     return true;
// }

// source : https://www.realtimerendering.com/raytracing/Ray%20Tracing%20in%20a%20Weekend.pdf
bool dielectric::scatter(const ray &r_in, hit_record &rec, vec3 &attenuation, ray &scattered) const {
    // write something here ...
    attenuation = vec3(1.0, 1.0, 1.0);
    vec3 outward_normal;
    vec3 reflected = reflect(r_in.direction(), rec.normal);
    vec3 refracted;
    float ni_over_nt;
    float reflect_prob;
    float cosine;
    if (dot(r_in.direction(), rec.normal) > 0) {
        outward_normal = -rec.normal;
        ni_over_nt = ref_idx;
        cosine = ref_idx * dot(r_in.direction(), rec.normal) / r_in.direction().length();
    } else {
        outward_normal = rec.normal;
        ni_over_nt = 1.0 / ref_idx;
        cosine = -dot(r_in.direction(), rec.normal) / r_in.direction().length();
    }
    if (refract(r_in.direction(), outward_normal, ni_over_nt, refracted)) {
        reflect_prob = schlick(cosine, ref_idx);
    } else {
        reflect_prob = 1.0;
    }
    if (drand48() < reflect_prob) {
        scattered = ray(rec.p, reflected);
    } else {
        scattered = ray(rec.p, refracted);
    }
    return true;
}

vec3 light::emitted() const {
    // write something here ...
    return vec3(1, 1, 1);
}

bool light::scatter(const ray &r_in, hit_record &rec, vec3 &attenuation, ray &scattered) const {
    // write something here ...
    return false;
}