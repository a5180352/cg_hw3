#ifndef CAMERAH
#define CAMERAH

#include "ray.hpp"
#include "util.hpp"

class camera {
public:
    camera() {}

    // vfov: vertical field of view in degree
    camera(vec3 lookfrom, vec3 lookat, vec3 up, float vfov, float aspect) {
        origin = lookfrom;
        // write something here ...
        // image info
        float vfov_rad = vfov * pi / 180.f;
        float height = tanf(vfov_rad / 2) * 2;
        float width = height * aspect;
        // camera coordinate space
        vec3 w = unit_vector(lookfrom - lookat);
        vec3 u = unit_vector(cross(up, w));
        vec3 v = unit_vector(cross(w, u));
        // image(texture) info in camera coordinate space
        horizontal = u * width;
        vertical = v * height;
        lower_left_corner = lookfrom - w - (horizontal / 2) - (vertical / 2);
    }

    inline ray get_ray(float s, float t) const {
        // write something here ...
        return ray(origin, (lower_left_corner + (horizontal * s) + (vertical * t)) - origin);
    }

    vec3 lower_left_corner; // texture coordinate (0, 0)
    vec3 horizontal;        // texture coordinate (1, 0)
    vec3 vertical;          // texture coordinate (0, 1)
    vec3 origin;            // camera position
};

#endif
